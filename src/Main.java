import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(final String[] arguments) {
        ArrayList<ProxyImage> images = new ArrayList<>();
        int n = 4;
        Random random = new Random();

        //lisätään kuvat
        for (int i = 0; i < n; i++) {
            images.add(
                    new ProxyImage(
                            "HiRes_%dMB_Photo%d"
                                    .formatted(random.nextInt(1024),i+1)
                    )
            );
        }

        //listataan sisältö
        images.forEach(ProxyImage::showData);

        //selataan satunnaisia kuvia
        for (int i = 0; i < n*2; i++) {
            ProxyImage randomImage = images.get(random.nextInt(images.size()));
            randomImage.displayImage();
        }
    }
}
